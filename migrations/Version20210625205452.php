<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210625205452 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE missions ADD compagnies_id INT NOT NULL');
        $this->addSql('ALTER TABLE missions ADD CONSTRAINT FK_34F1D47E5F9EE3CE FOREIGN KEY (compagnies_id) REFERENCES compagnies (id)');
        $this->addSql('CREATE INDEX IDX_34F1D47E5F9EE3CE ON missions (compagnies_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE missions DROP FOREIGN KEY FK_34F1D47E5F9EE3CE');
        $this->addSql('DROP INDEX IDX_34F1D47E5F9EE3CE ON missions');
        $this->addSql('ALTER TABLE missions DROP compagnies_id');
    }
}
