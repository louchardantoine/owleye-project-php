<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use function Sodium\add;

class CategoriesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $datas = array("Integration", "Developpement", "Reseau");

        foreach($datas as $i => $nom)
        {
            // On crée la compétence
            $liste_competences[$i] = new Categories();
            $liste_competences[$i]->setName($nom);
            // On la persiste
            $manager->persist($liste_competences[$i]);

            $this->addReference($nom, $liste_competences[$i]);
        }

        // On déclenche l'enregistrement
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded

    }
}
