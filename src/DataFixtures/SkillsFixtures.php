<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use App\Entity\Skills;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SkillsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $datas=[
             [
                 "name" => "wordpress",
                 "categories_id" => "Integration"
             ],
             [
                 "name" => "symfony",
                 "categories_id" => "Developpement"
             ],
             [
                 "name" => "PHP",
                 "categories_id" => "Developpement"
             ],
             [
                 "name" => "Easyadmin",
                 "categories_id" => "Developpement"
             ],
             [
                 "name" => "JAVA",
                 "categories_id" => "Reseau"
             ]
         ];

        foreach($datas as $i => $tabProducts) {
            // On crée la compétence
            $liste_competences[$i] = new Skills();
            $liste_competences[$i]->setName($tabProducts["name"]);

            // Gestion des categories
            $liste_competences[$i]->setCategories($this->getReference($tabProducts["categories_id"]));

            // On la persiste
            $manager->persist($liste_competences[$i]);
        }

        // On déclenche l'enregistrement
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}
