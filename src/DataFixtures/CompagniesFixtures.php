<?php

namespace App\DataFixtures;

use App\Entity\Compagnies;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CompagniesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $compagny = new Compagnies();
         $compagny->setName('Entreprise-1');
         $compagny->setCp('37000');
        $manager->persist($compagny);

        $compagny2 = new Compagnies();
        $compagny2->setName('Entreprise-2');
        $compagny2->setCp('37540');
        $manager->persist($compagny2);

        $compagny3 = new Compagnies();
        $compagny3->setName('Entreprise-3');
        $compagny3->setCp('44200');
        $manager->persist($compagny3);

        $compagny4 = new Compagnies();
        $compagny4->setName('Entreprise-4');
        $compagny4->setCp('37270');
         $manager->persist($compagny4);

        $manager->flush();
    }
}
