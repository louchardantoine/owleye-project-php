<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppUsers extends Fixture
{
    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager)
    {
         $user = new User();
         $user->setLastname('admin');
         $user->setFirstname('admin');
         $user->setEmail('adminadmin@owleye.fr');
         $user->setRoles(['ROLE_ADMIN']);
         $user->setAddress('0');
         $user->setCp('0');
         $user->setTelephone('0');
         $user->setCreatedAt(new \DateTime('now'));
        $password = $this->hasher->hashPassword($user, 'admin');
        $user->setPassword($password);
         $manager->persist($user);

        $commercial = new User();
        $commercial->setLastname('commercial');
        $commercial->setFirstname('commercial');
        $commercial->setEmail('commercialcommercial@owleye.fr');
        $commercial->setRoles(['ROLE_COMMERCIAL']);
        $commercial->setAddress('0');
        $commercial->setCp('0');
        $commercial->setTelephone('0');
        $commercial->setCreatedAt(new \DateTime('now'));
        $password = $this->hasher->hashPassword($commercial, 'commercial');
        $commercial->setPassword($password);
        $manager->persist($commercial);

        $collaborator = new User();
        $collaborator->setLastname('collaborator');
        $collaborator->setFirstname('collaborator');
        $collaborator->setEmail('collaboratorcollaborator@owleye.fr');
        $collaborator->setRoles(['ROLE_COLLABORATOR']);
        $collaborator->setAddress('0');
        $collaborator->setCp('0');
        $collaborator->setTelephone('0');
        $collaborator->setCreatedAt(new \DateTime('now'));
        $password = $this->hasher->hashPassword($collaborator, 'collaborator');
        $collaborator->setPassword($password);
        $manager->persist($collaborator);

        $candidat = new User();
        $candidat->setLastname('candidat');
        $candidat->setFirstname('candidat');
        $candidat->setEmail('candidatcandidat@owleye.fr');
        $candidat->setRoles(['ROLE_CANDIDAT']);
        $candidat->setAddress('0');
        $candidat->setCp('0');
        $candidat->setTelephone('0');
        $candidat->setCreatedAt(new \DateTime('now'));
        $password = $this->hasher->hashPassword($candidat, 'candidat');
        $candidat->setPassword($password);
        $manager->persist($candidat);

        $manager->flush();
    }
}
