<?php

namespace App\Controller\Admin;

use App\Controller\Admin\Filter\RolesFilter;
use App\Entity\User;
use App\Form\RoleType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use \EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstname', 'Prénom'),
            TextField::new('lastname', 'Nom'),
            TextField::new('address', 'Adresse'),
            TextField::new('cp', 'code postal'),
            TelephoneField::new('telephone'),
            EmailField::new('email')
                ->onlyOnIndex(),
            TextField::new('password')
                ->hideOnIndex()
                ->setPermission('ROLE_ADMIN')
                ->setHelp('Si vous souhaitez modifier votre mot de passe, effacez celui ci pour en réécrire un nouveau'),
            ChoiceField::new('roles', 'Roles')
                ->setPermission('ROLE_ADMIN')
                ->setChoices([
                                 'candidat' => 'ROLE_CANDIDAT',
                                 'Collaborateur' => 'ROLE_COLLABORATOR',
                                 'Commercial' => 'ROLE_COMMERCIAL',
                                 'Administrateur' => 'ROLE_ADMIN',
                                 'Independant' => 'ROLE_INDEPENDANT']
                )
                -> allowMultipleChoices( false )
                -> setFormType( RoleType :: class ),

            FormField::addPanel('Compétences'),
            AssociationField::new('skills', false)
                ->hideOnIndex(),

            DateTimeField::new('createdAt')
                ->onlyOnIndex(),
            DateTimeField::new('modifiedAt')
                ->onlyOnIndex()
        ];

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // the Symfony Security permission needed to manage the entity
            // (none by default, so you can manage all instances of the entity)
            // ->setEntityPermission('ROLE_COMMERCIAL')
            ->setPageTitle('edit', 'Editer un collaborateur')
            ->setPageTitle('index', 'Nos collaborateur')
            ->setPageTitle('detail', 'Profil de')
            ->overrideTemplate('crud/detail', 'dashboard/profile.html.twig');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->setPermission(Action::DELETE, 'ROLE_ADMIN')
            ->setPermission(Action::NEW, 'ROLE_ADMIN');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            // most of the times there is no need to define the
            // filter type because EasyAdmin can guess it automatically
            //->add(BooleanFilter::new('published'))
            ;
    }
}
