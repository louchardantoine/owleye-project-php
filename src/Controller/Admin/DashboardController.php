<?php

namespace App\Controller\Admin;

use App\Entity\Categories;
use App\Entity\Compagnies;
use App\Entity\Missions;
use App\Entity\Skills;
use App\Entity\User;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/", name="admin")
     */
    public function index(): Response
    {
        return $this->render('dashboard/index_admin.html.twig', [
            'new_users' => $this->userRepository->getUserCreate(),
            'number_new_users' => $this->userRepository->getNumberUserCreate(),
            'number_modify_users' => $this->userRepository->getNumberUserModify(),
            'number_create_candidats' => $this->userRepository->getNumberCandidat(),
            'new_candidats' => $this->userRepository->getCandidat()

        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Owleye Project')
            ->disableUrlSignatures();
    }

    public function configureMenuItems(): iterable
    {

        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::section('Gestions');
        yield MenuItem::linkToCrud('Utilisateur', 'fas fa-list', User::class)
            ->setPermission('ROLE_COMMERCIAL')
            ->setController(UserCrudController::class);
        yield MenuItem::linkToCrud('Compétences', 'fas fa-list', Skills::class)
            ->setPermission('ROLE_COMMERCIAL');
        yield MenuItem::linkToCrud('Catégories', 'fas fa-list', Categories::class)
            ->setPermission('ROLE_COMMERCIAL');
        yield MenuItem::linkToCrud('Entreptises', 'fas fa-list', Compagnies::class)
            ->setPermission('ROLE_COMMERCIAL');
        yield MenuItem::linkToCrud('Missions', 'fas fa-list', Missions::class)
            ->setPermission('ROLE_COMMERCIAL')
            ->setController(MissionsCrudController::class);

        yield MenuItem::section('Mon profil');
        yield MenuItem::linkToCrud('Mon profile', 'fas fa-list', User::class)
            ->setPermission('ROLE_COMMERCIAL')
            ->setAction('detail')
            ->setController(UserProfileCrudController::class);
        yield MenuItem::linkToCrud('Mes missions', 'fas fa-list', Missions::class)
            ->setPermission('ROLE_COLLABORATOR')
            ->setAction('index')
            ->setController(MissionsCurrentProfileCrudController::class);
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        // Usually it's better to call the parent method because that gives you a
        // user menu with some menu items already created ("sign out", "exit impersonation", etc.)
        // if you prefer to create the user menu from scratch, use: return UserMenu::new()->...
        return parent::configureUserMenu($user)
            // use the given $user object to get the user name
            ->setName($user->getFullName())
            // use this method if you don't want to display the name of the user
            //->displayUserName(false)

            // you can return an URL with the avatar image
            //->setAvatarUrl('https://...')
            //->setAvatarUrl($user->getProfileImageUrl())
            // use this method if you don't want to display the user image
            ->displayUserAvatar(false)
            // you can also pass an email address to use gravatar's service
            //->setGravatarEmail($user->getUserIdentifier())

            // you can use any type of menu item, except submenus
            ->addMenuItems([
                               MenuItem::linkToCrud('Mon Profile','fas fa-list' ,User::class)
                               ->setController(UserProfileCrudController::class)
                               ->setAction('detail'),
                               MenuItem::linkToCrud('Modifier mon Profile','fas fa-list' ,User::class)
                                   ->setController(UserProfileCrudController::class)
                                   ->setAction('edit')
                               ->setEntityId($this->getUser()->getId()),
                               MenuItem::section(),
                               MenuItem::linkToLogout('Logout', 'fa fa-sign-out'),
                           ]);
    }
}
