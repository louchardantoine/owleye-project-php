<?php

namespace App\Controller\Admin;

use App\Entity\Compagnies;
use App\Entity\Missions;
use App\Repository\MissionsRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

class MissionsCurrentProfileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Missions::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('users')
                ->setFormTypeOptions(['query_builder' => function (UserRepository $em) {
                    return $em->createQueryBuilder('u')
                        ->where('u = :user')
                        ->setParameter('user', $this->getUser())
                        ;
                }, 'required' => 'required']),
            AssociationField::new('compagnies', 'Entreprises')
                ->setRequired(true),
            TextEditorField::new('description'),
            DateTimeField::new('beginAt', 'Début de mission'),
            DateTimeField::new('EndAt', 'Fin de mission'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->setPermission(Action::DELETE, 'ROLE_ADMIN')
            ->setPermission(Action::NEW, 'ROLE_COLLABORATOR')
            ->setPermission(Action::EDIT, 'ROLE_ADMIN');
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->andWhere('entity.users = :user');
        $qb->setParameter('user', $this->getUser())
            ->getQuery()
            ->getResult();

        return $qb;
    }

}
