<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\RoleType;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UserProfileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstname', 'Prénom'),
            TextField::new('lastname', 'Nom'),
            TextField::new('address', 'Adresse'),
            TextField::new('cp', 'code postal'),
            TelephoneField::new('telephone'),
            EmailField::new('email')
                ->onlyOnIndex(),
            TextField::new('password')
                ->hideOnIndex()
                ->setPermission('ROLE_CANDIDAT')
                ->setHelp('Si vous souhaitez modifier votre mot de passe, effacez celui ci pour en réécrire un nouveau'),
            ChoiceField::new('roles', 'Roles')
                ->setPermission('ROLE_ADMIN')
                //->autocomplete()
                //->allowMultipleChoices()
                ->setChoices([
                                 'candidat' => 'ROLE_CANDIDAT',
                                 'Collaborateur' => 'ROLE_COLLABORATOR',
                                 'Commercial' => 'ROLE_COMMERCIAL',
                                 'Administrateur' => 'ROLE_ADMIN',
                                 'Independant' => 'ROLE_INDEPENDANT']
                )
                -> allowMultipleChoices( false )
                -> setFormType( RoleType :: class ),

            FormField::addPanel('Compétences'),
            AssociationField::new('skills', false)
                ->hideOnIndex(),

            DateTimeField::new('createdAt')
                ->onlyOnIndex(),
            DateTimeField::new('modifiedAt')
                ->onlyOnIndex()
        ];

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // the Symfony Security permission needed to manage the entity
            // (none by default, so you can manage all instances of the entity)
            // ->setEntityPermission('ROLE_COMMERCIAL')
            ->setPageTitle('edit', 'Modifier mon profil')
            ->setPageTitle('detail', 'Mon Profile')
            ->overrideTemplate('crud/detail', 'dashboard/my-profile.html.twig')
            ->overrideTemplate('crud/index', 'dashboard/my-profile.html.twig');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->setPermission(Action::DELETE, 'ROLE_ADMIN')
            ->setPermission(Action::NEW, 'ROLE_ADMIN');
    }


    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->andWhere('entity = :user');
        $qb->setParameter('user', $this->getUser())
            ->getQuery()
            ->getResult();

        return $qb;
    }


}
