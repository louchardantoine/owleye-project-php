<?php


namespace App\eventSubscriber;


use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    /** @var UserPasswordEncoderInterface */
    private UserPasswordEncoderInterface $passwordEncoder;
    //private MailerInterface $mailer;


    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->passwordEncoder = $passwordEncoder;

    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setUserAddedDate'],
            BeforeEntityUpdatedEvent::class => ['setUpdateUserAddedDate'],
        ];
    }

    public function setUserAddedDate(BeforeEntityPersistedEvent $event){
        $entity = $event->getEntityInstance();

        if(!($entity instanceof User)){
            return;
        }

        $user = $event->getEntityInstance();
        if ($user->getPassword()) {
            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        }

        $now = new \DateTime('now');
        $entity->setCreatedAt($now);

        $mail = $entity->getLastname() . $entity->getFirstname() . '@owleye' . '.fr' ;
        $entity-> setEmail($mail);

        /*TODO verifier si le User n'a pas deux missions qui s'entrecroisent*/
    }

    public function setUpdateUserAddedDate(BeforeEntityUpdatedEvent $event){
        $entity = $event->getEntityInstance();

        if(!($entity instanceof User)){
            return;
        }

        /*$user = $event->getEntityInstance();
        if ($user->getPassword() ) {

            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        }*/

        $now = new \DateTime('now');
        $entity->setModifiedAt($now);

        $mail = $entity->getLastname() . $entity->getFirstname() . '@owleye' . '.fr' ;
        $entity-> setEmail($mail);
    }


}