<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @return User[] Returns an array of User objects
     */

    public function getUserCreate()
    {
        return $this->createQueryBuilder('u')
            ->where('u.createdAt > :val')
            ->setParameter('val', new \DateTime('-1 day'))
            ->andWhere('LENGTH(u.roles) > 2 ')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getNumberUserCreate()
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id) AS value')
            ->where('u.createdAt > :val')
            ->setParameter('val', new \DateTime('-1 day'))
            ->andWhere('LENGTH(u.roles) > 2 ')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getUserModify()
    {
        return $this->createQueryBuilder('u')
            ->where('u.modifiedAt > :val')
            ->setParameter('val', new \DateTime('-1 day'))
            ->getQuery()
            ->getResult()
            ;
    }

    public function getNumberUserModify()
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id) AS value')
            ->where('u.modifiedAt > :val')
            ->andwhere('u.modifiedAt IS NOT NULL')
            ->setParameter('val', new \DateTime('-1 day'))
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getCandidat()
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles > :val')
            ->setParameter('val', new \DateTime('-1 day'))
            ->andWhere('LENGTH(u.roles) <= 2')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getNumberCandidat()
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id) as value')
            ->where('u.createdAt > :val')
            ->setParameter('val', new \DateTime('-1 day'))
            ->andWhere('LENGTH(u.roles) <= 2')
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
