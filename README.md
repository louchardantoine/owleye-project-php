## INITIALISATION DU PROJET

### ETAPES D'INITIALISATION

1- Copier-Coller le dossier .env et renommé le en .env.local

2- créer une base de donnée en local puis aller dans le fichier .env.local afin de modifier les données de la base 
ciblée;

3- éxecuter la commande -- symfony console doctrine:migrations:migrate --;

4- éxecuter la commande suivant --- symfony console doctrine:fixtures:load --- afin de créer des données dans la base.

5- Il ne restera plus qu'à créer des missions qui ne sont pas crées en FIXTURES.

###logs des utilisateurs en fixtures

1- EMAIL: candidatcandidat@owleye.fr / MDP: candidat

2- EMAIL: collaboratorcollaborator@owleye.fr / MDP: collaborator

3- EMAIL: commercialcommercial@owleye.fr / MDP: commercial

4- EMAIL: adminadmin@owleye.fr / MDP: admin

### A SAVOIR

Chaque nouvel utilisateur créé crééra son email comme le schema suivant : nomprenénom@owleye.fr.

Vous utiliserez donc ce schema pour les prochaines connexions des utlisateurs créés
